const {emails} = require("./../../db")
const CONFIG = require("./../../config/config")

const draftMail = async (req, res,next) => {
    try {
        await emails.create({
            senderEmail: CONFIG.MAIL.EMAIL,
            receiverEmail: req.body.receiver,
            subject: req.body.subject,
            body: req.body.body,
            isDraft: true,
            draftedAt: new Date().toISOString()
        })
        res.send({
            message: 'saved as draft!',
        });
        next();
    } catch (err) {
        console.log('err');
        res.send(err);
        next(err);
    }
};

module.exports = draftMail;

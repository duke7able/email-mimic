const {emails} = require("./../../db")
const CONFIG = require("./../../config/config")

const listAllMail = async (req, res,next) => {
    try {
        const requestedOffset = !req.query.offset && parseInt(req.query.offset) !== 0 ? 0 : parseInt(req.query.offset)
        const mails = await emails.findAndCountAll({
            offset: requestedOffset,
            limit: parseInt(req.query.pageSize) || 10,
            where: {
                isDeleted: false,
                senderEmail: CONFIG.MAIL.EMAIL
            },
            order: [
                ['updatedAt', 'DESC']
            ],
            attributes: ['id','senderEmail', 'receiverEmail', 'body', 'subject', 'isDraft', 'draftedAt', 'updatedAt']
        })
        mails.rows.forEach(element => {
            console.log('mails',element.dataValues)
        });
        res.send({
            data: mails,
        });
        next();
    } catch (err) {
        console.log('err');
        res.send(err);
        next(err);
    }
};

module.exports = listAllMail;

const { Mailer } = require("./../../utils/mail")
const {emails} = require("./../../db")
const CONFIG = require("./../../config/config")

const sendMail = async (req, res,next) => {
    try {
        const mailer = new Mailer(CONFIG.MAIL.EMAIL,CONFIG.MAIL.PASSWORD)
        await mailer.sendEmail(CONFIG.MAIL.EMAIL,req.body.receiver,req.body.subject,req.body.body);
        await emails.create({
            senderEmail: CONFIG.MAIL.EMAIL,
            receiverEmail: req.body.receiver,
            subject: req.body.subject,
            body: req.body.body
        })
        res.send({
            message: 'Mail sent!',
        });
        next();
    } catch (err) {
        console.log('err');
        res.send(err);
        next(err);
    }
};

module.exports = sendMail;

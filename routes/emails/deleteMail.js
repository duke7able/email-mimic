const {emails} = require("./../../db")
const CONFIG = require("./../../config/config")

const deleteMail = async (req, res,next) => {
    try {
        const softDeleteEmailId = parseInt(req.body.deleteMailId)
        if (softDeleteEmailId < 0) {
            throw new Error('Invalid Mail Id')
        }
        await emails.update({
            isDeleted: true,
            deletedAt: new Date().toISOString()
        },{
            where: {
                id: softDeleteEmailId,
                senderEmail: CONFIG.MAIL.EMAIL
            }
        })
        res.send({
            message: 'Mail Deleted!',
        });
        next();
    } catch (err) {
        console.log('err');
        res.send(err);
        next(err);
    }
};

module.exports = deleteMail;

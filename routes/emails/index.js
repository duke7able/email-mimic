const custom = require('../../middleware/custom');

const EmailsControllers = router => {
    router.post('/v1/sendMail', [custom], require('./sendMail'));
    router.post('/v1/draftMail', [custom], require('./draftMail'));
    router.get('/v1/listAllMail', [custom], require('./listAllMail'));
    router.delete('/v1/deleteMail', [custom], require('./deleteMail'));
}

module.exports = EmailsControllers;

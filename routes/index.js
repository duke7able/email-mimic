const express = require('express');

const router = express.Router();

require('./emails/index')(router);

module.exports = router;

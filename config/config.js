require('dotenv').config("./../.env");
module.exports = {
    ENVIRONMENT: process.env.NODE_ENV,
    DB: {
        HOST: process.env.DB_HOST,
        USERNAME: process.env.DB_USERNAME,
        DATABASE: process.env.DB_DATABASE,
        PASSWORD: process.env.DB_PASSWORD,
        PORT: process.env.DB_PORT,
    },
    APP: {
        PORT: process.env.API_PORT
    },
    MAIL: {
        EMAIL: process.env.MAIL_EMAIL,
        PASSWORD: process.env.MAIL_PASSWORD
    }
}
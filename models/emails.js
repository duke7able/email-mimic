const db = require("../db")

/**
 * @description table name: user , elements:username, email, pass, name, mob, budget
 */
const emails = db.define('email', {
    id: {
        type: Sequelize.DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    senderEmail: Sequelize.DataTypes.STRING,
    receiverEmail: Sequelize.DataTypes.STRING,
    body: Sequelize.DataTypes.STRING,
    subject: Sequelize.DataTypes.STRING,
});

module.exports = {
    emails
}
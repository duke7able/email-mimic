# email-mimic

To run use command npm run start
for swagger you would need to call every api once from postman or anything else

## For enabling mail from gmail turn on access for less secure apps
https://www.google.com/settings/security/lesssecureapps
### What it does not have since its not required yet, but could be and should be added if to be added on prod
- migrations folder with migrations for sequelize 
- auth with or without passport
- models folder with models when we have multiple tables
- Auth since currently it does not need auth
- code to call swagger all sample apis, to generate swagger api when server starts
- pm2, definately should be added
module.exports.responseWriter = (res, status, responseObj) => {
    let token = null;
    try {
        // Only if token value is present extraction must be attempted
        if (responseObj.hasOwnProperty('token')) {
            token = responseObj.token;
            return res
                .status(status)
                .header('Authorization', token)
                .json(responseObj);
        }
    } catch (err) {
        console.log(`Error occurred ${err.stack}`);

        return res
            .status(500)
            .json({ message: `Internal Error ${err.message}` });
    }
    return res.status(status).json(responseObj);
};

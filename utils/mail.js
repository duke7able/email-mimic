const nodemailer = require("nodemailer");

class Mailer {
    constructor (username, password) {
        this.transporter = nodemailer.createTransport({
            service: "gmail",
            auth: {
                user: username,
                pass: password
            }
        });
    }

    sendEmail (fromEmail, toEmail, subject, body) {
        return new Promise((resolve,reject) => {
            const mailOptions = {
                from: fromEmail,
                to: toEmail,
                subject: subject,
                text: body
            };
            this.transporter.sendMail(mailOptions, (error, info) => {
                if (error) {
                    console.log("error",error);
                    return reject(error);
                }
                return resolve("Mail Sent Successfully");
            });
        })
    }
}

module.exports = {
    Mailer
};

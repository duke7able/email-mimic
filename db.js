const Sequelize = require('sequelize');

require('dotenv').config();

/**
 * @description database tables using Sequelize
 */
const db = new Sequelize({
    host: process.env.DB_HOST,
    username: process.env.DB_USERNAME,
    database: process.env.DB_DATABASE,
    password: process.env.DB_PASSWORD,
    port: process.env.DB_PORT,
    dialect: 'mysql',
    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
    }
});


/**
 * @description table name: user , elements:username, email, pass, name, mob, budget
 */
const emails = db.define('email', {
    id: {
        type: Sequelize.DataTypes.INTEGER.UNSIGNED,
        primaryKey: true,
        autoIncrement: true,
    },
    senderEmail: Sequelize.DataTypes.STRING,
    receiverEmail: Sequelize.DataTypes.STRING,
    body: Sequelize.DataTypes.STRING,
    subject: Sequelize.DataTypes.STRING,
    deletedAt: Sequelize.DataTypes.DATE,
    isDeleted: {
        type: Sequelize.DataTypes.BOOLEAN,
        defaultValue: false
    },
    isDraft: {
        type: Sequelize.DataTypes.BOOLEAN,
        defaultValue: false
    },
    draftedAt: Sequelize.DataTypes.DATE
});


db.sync().then(async () => {
    await emails.sync();
    console.log('Database is ready');
});



// In live app we should export db object so that we can create migrations and models
module.exports = {
    db,
    emails
};

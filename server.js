const express = require('express');
const bp = require('body-parser');
const {db} = require("./db")
const cors = require('cors')

const CONFIG = require('./config/config')
const app = express();
const expressOasGenerator = require('express-oas-generator');
expressOasGenerator.handleResponses(app, {});
// Check swagger api doc on http://localhost:${CONFIG.APP.PORT}
// Make sure to call next() in middlewares and routes.

app.use(cors())
app.use(bp.urlencoded({ extended: true }));
app.use(bp.json());

app.use('/', require('./routes/index'));

app.listen(process.env.API_PORT, function () {
    console.log(`Server started on http://localhost:${CONFIG.APP.PORT}`);
    expressOasGenerator.handleRequests();
});
